<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Validations\ProductValidation;

class ProductTest extends TestCase
{
    public static $product;

    public function setup()
    {
        parent::setUp();
        self::$product = factory(App\Product::class)->make();
    }

    public function testLmShouldNotBeBlank()
    {
        self::$product->lm = '';
        $this->assertFalse(
            $this->productValidation(self::$product['attributes'])
        );
    }

    public function testLmShouldNotBeValid()
    {
        self::$product->lm = 'asda';
        $this->assertFalse(
            $this->productValidation(self::$product['attributes'])
        );
    }

    public function testNameShouldBePresent()
    {
        self::$product->name = '';
        $this->assertFalse(
            $this->productValidation(self::$product['attributes'])
        );
    }

    public function testFreeShippingShouldNotBeValid()
    {
        self::$product->free_shipping = 3;
        $this->assertFalse(
            $this->productValidation(self::$product['attributes'])
        );
    }

    public function testPriceShouldNotBeValid()
    {
        self::$product->price = 'notPrice';
        $this->assertFalse(
            $this->productValidation(self::$product['attributes'])
        );
    }

    public function testCategoryIdShouldBeInteger()
    {
        self::$product->category_id = 'a';
        $this->assertFalse(
            $this->productValidation(self::$product['attributes'])
        );
    }

    public function productValidation($attributes)
    {
        $validation = new ProductValidation($attributes);
        return $validation->isValid();
    }
}
