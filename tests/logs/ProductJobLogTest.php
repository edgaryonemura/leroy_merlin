<?php

use App\Jobs\ImportProducts;
use Illuminate\Http\UploadedFile;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProductJobLogTest extends TestCase
{
    public function setup()
    {
        parent::setUp();
        $this->name = str_random(8).'.xlsx';
        $this->path = sys_get_temp_dir().'/'.$this->name;
        $this->mime = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
    }

    public function testLogFormation()
    {
        $job = $this->job(
            copy($this->fixtures('products_fails.xlsx'), $this->path)
        );

        $message = "[PRODUCT_XLS] An error occurred while trying to store the product";
        $this->assertEquals($job->log([]), $message);

        $this->assertFalse($job->handle());
    }
    public function fixtures(string $file)
    {
        return  $this->fixtures = base_path().'/tests/fixtures/'.$file;
    }
    public function job()
    {
        $file = new UploadedFile($this->path, $this->name, $this->mime, filesize($this->path), null, true);
        return new ImportProducts($file);
    }
}
