<?php

use App\Jobs\ImportProducts;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Log;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ImportProductsTest extends TestCase
{
    use DatabaseTransactions;

    public function setup()
    {
        parent::setUp();
        $this->name = str_random(8).'.xlsx';
        $this->path = sys_get_temp_dir().'/'.$this->name;
        $this->mime = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
    }
    public function testXlsWithValidProducts()
    {
        $job = $this->job(
            copy($this->fixtures('products.xlsx'), $this->path)
        );
        $this->assertTrue($job->handle());
    }

    public function testXlsWithWrongProducts()
    {
        $this->mockLog(["lm" => null,"name" => "Chave de Fenda X","free_shipping" => 0.0,"description" => "Chave de fenda simples","price" => "20.00"]);
        $this->mockLog(["lm" => 1010,"name" => "Luvas de Proteção","free_shipping" => 0.0,"description" => "Luva de proteção básica","price" => "asd"]);
        $this->mockLog(["lm" => 1055,"name" => null,"free_shipping" => 1.0,"description" => "Mascara proteção básica","price" => "3.80"]);

        $job = $this->job(
            copy($this->fixtures('products_fails.xlsx'), $this->path)
        );
        $this->assertFalse($job->handle());
    }

    public function fixtures(string $file)
    {
        return  $this->fixtures = base_path().'/tests/fixtures/'.$file;
    }
    public function mockLog($attributes)
    {
        Log::shouldReceive('error')
            ->with('[PRODUCT_XLS] An error occurred while trying to store the product', $attributes)
            ->andReturn('[PRODUCT_XLS] An error occurred while trying to store the product');
    }
    public function job()
    {
        $file = new UploadedFile($this->path, $this->name, $this->mime, filesize($this->path), null, true);
        return new ImportProducts($file);
    }
}
