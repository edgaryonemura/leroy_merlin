<?php

use App\Jobs\ImportProducts;
use Illuminate\Http\UploadedFile;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProductsControllerTest extends TestCase
{
    use DatabaseTransactions;

    public function setup()
    {
        parent::setUp();
        $this->name = str_random(8).'.xlsx';
        $this->path = sys_get_temp_dir().'/'.$this->name;
        $this->mime = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
    }

    public function testWithValidXls()
    {
        copy($this->fixtures('products.xlsx'), $this->path);

        $file = $this->uploadFile();

        $this->expectsJobs(ImportProducts::class);

        $response = $this->call('POST', '/products', [], [], ['productxls' => $file]);
        $this->assertEquals(302, $response->status());
    }

    public function testWithInvalidXls()
    {
        copy($this->fixtures('products_fails.xlsx'), $this->path);

        $file = $this->uploadFile();

        $this->doesntExpectJobs(ImportProducts::class);

        $response = $this->call('POST', '/products', [], [], ['productxls' => $file]);
        $this->assertEquals(302, $response->status());
    }

    public function testUpdate()
    {
        $product = $this->createProduct();

        $response = $this->call('PATCH', "/products/{$product->id}",
            [
                'lm' => '2000',
                'name' => 'Drill',
                'free_shipping' => 0,
                'description' => 'Power Drill',
                'price' => 100.50
            ]
        );

        $changedProduct = App\Product::find($product->id);
        $this->assertEquals($changedProduct->name, 'Drill');
        $this->assertEquals(200, $response->status());
    }

    public function testUpdateWithInvalidValues()
    {
        $product = $this->createProduct();

        $response = $this->call('PATCH', "/products/{$product->id}",
            [
                'lm' => 'abcd',
                'name' => 'Drill',
                'free_shipping' => 0,
                'description' => 'Power Drill',
                'price' => 'xyz'
            ]
        );

        $changedProduct = App\Product::find($product->id);
        $this->assertNotEquals($changedProduct->name, 'Drill');
        $this->assertEquals(302, $response->status());
    }

    public function testDelete()
    {
        $product = $this->createProduct();

        $response = $this->call('DELETE', "/products/{$product->id}",
            [
                'lm' => 'abcd',
                'name' => 'Drill',
                'free_shipping' => 0,
                'description' => 'Power Drill',
                'price' => 'xyz'
            ]
        );

        $product = App\Product::find($product->id);
        $this->assertEquals($product, null);
        $this->assertEquals(302, $response->status());
    }

    public function createProduct()
    {
        return factory(App\Product::class)->create([
            'lm' => 2000,
            'name' => 'Hammer XT',
            'free_shipping' => 0,
            'description' => 'A new hammer',
            'price' => 100.32
        ]);
    }
    public function fixtures(string $file)
    {
        return  $this->fixtures = base_path().'/tests/fixtures/'.$file;
    }
    public function uploadFile()
    {
        return new UploadedFile($this->path, $this->name, $this->mime, filesize($this->path), null, true);
    }
}
