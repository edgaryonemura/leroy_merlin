# Setup

PHP >=5.6.4  
Laravel 5.3

Clone the project to your workspace

```sh
$ git clone https://edgaryonemura@bitbucket.org/edgaryonemura/leroy_merlin.git
```
Install all dependecies

```sh
$ cd leroy_merlin
$ composer install
```

Copy and rename the .env.example to .env and add your environment.

```sh
$ cp .env.example .env
```

And then run

```sh
$ php artisan migrate
```

To generate Encryption Key(APP_KEY) run

```sh
$ php artisan key:generate
```

Start the server

```sh
$ php artisan serve
```

Running the tests
```sh
$ php vendor/bin/phpunit
```
