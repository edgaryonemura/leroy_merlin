<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Product::class, function(Faker\Generator $faker) {
    return [
        'lm' => $faker->numberBetween($min = 1000, $max = 9999),
        'name' => $faker->word,
        'free_shipping' => $faker->numberBetween($min = 0, $max = 1),
        'description' => $faker->text(100),
        'price' => $faker->randomNumber(3),
        'category_id' => $faker->randomNumber(1)
    ];
});
