<?php

namespace App\Jobs;

use Excel;
use App\Product;
use App\Category;
use App\Validations\ProductValidation;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\UploadedFile;

class ImportProducts implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    const CATEGORY = 0;
    const CATEGORY_NAME = 2;
    const PRODUCTS = 2;
    const COLUMNS_NAME = 1;

    protected $file;
    private $success = true;
    protected $error_message = '[PRODUCT_XLS] An error occurred while trying to store the product';

    /**
     * Create a new job instance.
     *
     * @param  string  $file
     * @return void
     */
    public function __construct($file)
    {
        $this->file = $file;
    }

    /**
     * Execu/te the job.
     *
     * @return void
     */
    public function handle()
    {
        Excel::load($this->file, function($xls) {

            $products = $this->xlsFilter($xls);

            $columns = $products[self::COLUMNS_NAME];

            $category = Category::firstOrCreate(
                ["name" => $products[self::CATEGORY][self::CATEGORY_NAME]]
            );

            unset($products[self::CATEGORY]);
            unset($products[self::COLUMNS_NAME]);

            foreach ($products as $product) {

                $attributes = $this->combine($columns, $product);

                $validator = new ProductValidation($attributes);

                if ($validator->isValid()) {
                    $attributes['category_id'] = $category->id;

                    Product::updateOrCreate(
                        ["lm" => $attributes['lm']], $attributes
                    );
                } else {
                    $this->log($attributes);
                    $this->success = false;
                }
            }
        });
        return $this->isSuccessful();
    }
    /**
     * Filter Product Xls
     *
     * @return array $xls
     */
    public function xlsFilter($xls)
    {
        return array_values(
            array_filter($xls->noHeading()->toArray()[0])
        );
    }

    /**
     * Combine two arrays
     * @param  array $columns
     * @param  array $product
     * @return array
     */
    public function combine($columns, $product)
    {
        foreach ($columns as $key => $name) {
            $attributes[$name] = $product[$key] ?? null;
        }
        return $attributes;
    }

    public function log($attributes)
    {
        Log::error($this->error_message, $attributes);
        return $this->error_message;
    }

    public function isSuccessful()
    {
        return $this->success;
    }
}
