<?php

namespace App\Validations;

use Validator;

class Validation
{
    private $errors;

    public function __construct($attributes)
    {
        $this->validation = Validator::make(
            $attributes, $this->rules, $this->messages
        );
    }

    public function isValid()
    {
        return !$this->validation->fails();
    }

    public function errors()
    {
        return $this->validation->errors();
    }
}
