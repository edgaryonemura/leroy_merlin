<?php

namespace App\Validations;

class ProductValidation extends Validation
{
    protected $rules = [
        'lm' => 'required|integer',
        'name' => 'required',
        'free_shipping' => 'integer|between:0,1',
        'price' => 'required|numeric',
        'category_id' => 'integer'
    ];
    protected $messages = [];
}
