<?php

namespace App\Validations;

class XlsValidation extends Validation
{
    protected $rules = [
        'productxls' => 'required|mimetypes:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    ];
    protected $messages = [
        'productxls.required' => 'The File field is required.',
        'productxls.mimetypes' => 'The File must be one of the following types: :values.',
    ];
}
