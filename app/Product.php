<?php

namespace App;

use Validator;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'lm', 
        'name', 
        'free_shipping', 
        'description', 
        'price', 
        'category_id',
        'updated_at',
        'created_at'
    ];

    /**
     * Get the category that owns the product.
     */
    public function category()
    {
        return $this->belongsTo('App\Category');
    }
}
