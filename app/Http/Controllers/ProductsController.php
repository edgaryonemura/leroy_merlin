<?php

namespace App\Http\Controllers;

use Excel;
use App\Product;
use App\Jobs\ImportProducts;
use App\Validations\XlsValidation;
use App\Validations\ProductValidation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::paginate(10);
        return view('products.index', compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = new XlsValidation($request->all());

        if ($validation->isValid()) {
            dispatch(new ImportProducts(
                $request->file('productxls')->path()
            ));

            flash('Job was add to queue was successful', 'success');
            return redirect()->route('products.index');
        }
        return back()->withErrors($validation->errors());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        return view('products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        return view('products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $validation = new ProductValidation($request->input());

        if ($validation->isValid()) {
            $product->update($request->all());

            flash('Product updated', 'success');

            return view('products.show', compact('product'));
        }
        return back()->withErrors($validation->errors());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::destroy($id);

        flash('Product deleted successfully', 'success');
        return redirect()->route('products.index');
    }
}
