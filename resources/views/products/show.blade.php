@extends('layouts.app')

@section('title', 'Product')

@section('content')
  <h1>{{ $product->lm }} - {{ $product->name }}</h1>

  <strong>Free Shipping</strong>: {{ $product->free_shipping ? 'Yes' : 'No' }}<br>
  <strong>Price</strong>: R$ {{ $product->price }}<br><br>
  <strong>Description:</strong><br>
  <p>{{ $product->description }}</p><br>

  <div class="row">
    <div class="col-md-2">
      <a href="{{ url()->previous() }}" class="btn btn-default">Back</a>
    </div>
    <div class="col-md-2">
      <form action="/products/{{ $product->id }}" method="POST">
        {{ method_field('DELETE') }}
        {{ csrf_field() }}
        <button type="submit" class="btn btn-danger">DELETE</button>
      </form>
    </div>
  </div>
@endsection
