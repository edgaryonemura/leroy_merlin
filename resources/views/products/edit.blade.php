@extends('layouts.app')

@section('title', 'Edit Product')

@section('content')
  <form action="/products/{{ $product->id }}" method="POST">
    {{ method_field('PATCH') }}
    {{ csrf_field() }}

    <div class="checkbox">
      <label>
        <input type="hidden" name="free_shipping" value="0">
        <input type="checkbox" name="free_shipping" value="1"
          {{ $product->free_shipping == 1 ? "checked" : "" }}> Free Shipping?
      </label>
    </div>

    <div class="form-group">
      <label for="lm">lm</label>
      <input type="text" class="form-control" id="lm" name="lm" placeholder="lm" value="{{ $product->lm }}">
    </div>
    <div class="form-group">
      <label for="name">Name</label>
      <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{ $product->name }}">
    </div>

    <div class="form-group">
      <label for="price">Price</label>
      <input type="text" class="form-control" id="price" name="price" placeholder="price" value="{{ $product->price }}">
    </div>
    <div class="form-group">
      <label for="price">Description</label>
      <textarea class="form-control" id="description" name="description" placeholder="description">{{ $product->description }}</textarea>
    </div>
    <button type="submit" class="pull-right btn btn-primary">Submit</button>
    <a href="{{ url()->previous() }}" class="pull-left btn btn-default">Back</a>
  </form>
@endsection
