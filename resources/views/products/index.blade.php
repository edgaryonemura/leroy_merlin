@extends('layouts.app')

@section('title', 'Products')

@section('content')

  <form action="/products" method="POST" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="form-group">
      <label for="productxls">Products XLSX</label>
      <input type="file" name="productxls" id="productxls">
    </div>
    <button type="submit" class="btn btn-default">
      Upload & Insert
    </button>
  </form>

  <table class="table">
    <thead>
      <tr>
        <th>lm</th>
        <th>Name</th>
        <th>Free Shipping</th>
        <th>Description</th>
        <th>Price</th>
        <th>Category</th>
        <th colspan="2"></th>
      </tr>
    </thead>
    <tbody>
      @foreach ($products as $product)
        <tr>
          <td>{{ $product->lm }}</td>
          <td>{{ $product->name }}</td>
          <td>{{ $product->free_shipping ? 'Yes' : 'No' }}</td>
          <td>{{ $product->description }}</td>
          <td>R$ {{ $product->price }}</td>
          <td>{{ $product->category->name }}</td>
          <td>
            <a href="{{ action('ProductsController@edit', ['id' => $product->id]) }}" class="glyphicon glyphicon-pencil"></a>
          </td>
          <td>
            <a href="{{ action('ProductsController@show', ['id' => $product->id]) }}" class="glyphicon glyphicon-eye-open"></a>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
  <div class="pagination"> {{ $products->links() }} </div>
@endsection
